<?php require_once('header_footer/header.php'); ?>

    <main>
        Selamat datang dan selamat belajar PHP 

        <h2>Implode dan Explode</h2>

        <?php
            //implode(separator, array) array menjadi string
            $pekerjaan = ['programmer', 'designer', 'manager'];
            echo implode(", ", $pekerjaan);

            echo '<br>';

            //explode(jenis separator, array) string menjadi array
            $pelajaran = 'html css javascript php';
            print_r(explode(" ", $pelajaran));
        ?>

        <h2>Fungsi Date</h2>
        <?php
            echo date('d - M - Y');
        ?>

        <h2>Trim dan Strip Tags</h2>
        <?php
            //trim
            $text = " ini adalah input dari user ";
            echo "sebelum".$text."di sini";
            echo'<br>';
            //trim ltrim untuk left, dan rtrim untuk right
            echo "sebelum".trim($text)."di sini";

            //strip
            $text2 = "<script>alert('Hai cuy')</script>";
            //mencegah muncul terus menerus dieksekusi dan dieksekusi sebagai string biasa
            echo strip_tags($text2);
        ?>
    </main>

<?php require_once('header_footer/footer.php'); ?>