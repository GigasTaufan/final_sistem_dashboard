<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";

// LATIHAN
// Menambahkan autocomplete
require_once ABSPATH . "php/jqAutocomplete.php";

// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");


// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT MemberID id, CardID, LastName, FirstName, BirthDate, Address, City, Country, AdmisionFee FROM member';
// Set the table to where you add the data
$grid->table = 'member';
$grid->setPrimaryKeyId('MemberID');
$grid->serialKey = false;
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
// LATIHAN
// hidden untuk menyembunyikan field tersebut dari form
$Model = array(
	// MemberID di sini di hidden
	// selain yang dihidden akan ditampilakn
	array("name" => "MemberID", "label" => "Member ID", "width" => 50, "hidden" => true),
	array("name" => "CardID", "label" => "Card ID", "width" => 25),
	array("name" => "LastName", "label" => "Last Name", "width" => 50),
	array("name" => "FirstName", "label" => "First Name", "width" => 50),
	array("name" => "BirthDate", "label" => "Birth Date", "width" => 35),
	array("name" => "Address", "label" => "Address", "width" => 100),
	array("name" => "Country", "label" => "Country", "width" => 75),
	array("name" => "City", "label" => "City", "width" => 100),
	array("name" => "PostalCode", "label" => "PostalCode", "width" => 25, "hidden" => true),
	array("name" => "HomePhone", "label" => "HomePhone", "width" => 25, "hidden" => true),
	// currency untuk keuangan
	array(
		"name" => "AdmisionFee", "width" => 80,
		"formatter" => "currency",
		"formatoptions" => array("decimalPlaces" => 1, "thousandsSeparator" => ",", "prefix" => "Rp ", "suffix" => ""), "sorttype" => "currency"
	)

);
// Let the grid create the model
$grid->setColModel($Model);
// Set the url from where we obtain the data
$grid->setUrl('grid.php');
// Set some grid options
$grid->setGridOptions(array(
	"caption" => "MEMBER",
	"rowNum" => 10,
	"rowList" => array(10, 20, 30, 50, 100),
	"height" => 'auto',
	"hoverrows" => true,
	"autowidth" => true,
	"rownumbers" => true,
	"sortname" => "MemberID",
	"sortorder" => "asc",
	"altRows" => true,
	"altclass" => 'myAltRowClass'

));
// The primary key should be entered
$grid->setDbDate('Y-m-d');
$grid->setDbTime('Y-m-d H:i:s');
$grid->setUserDate('Y-m-d');
$grid->setUserTime('Y-m-d');

$grid->setColProperty('MemberID', array("editrules" => array("required" => false)));
$grid->setColProperty(
	'BirthDate',
	array(
		"formoptions" => array("label" => "BirthDate"), "editable" => true, "formatter" => "date", "datefmt" => 'd/m/Y', "formatoptions" => array("srcformat" => "Y-m-d H:i:s", "newformat" => "Y-m-d"),
		"editrules" => array("edithidden" => true, "required" => true, "readonly" => false),
		"editoptions" => array("tabindex" => 7, "required" => true, "readonly" => false, "dataInit" =>
		"js:function(elm){setTimeout(function(){
                          jQuery(elm).datepicker({dateFormat:'yy-m-d'});
                             jQuery('.ui-datepicker').css({'zIndex':'1200','font-size':'75%'});},100);}")
	)
);

// LATIHAN
// set autocomplete. Serch for name and ID, but select a ID
// set it only for editing and not on serch
// $grid->setAutocomplete("Country", false, "SELECT DISTINCT Country FROM customers WHERE Country LIKE ? ORDER BY Country", null, true, false);
// ======================================================================================================================
// depent list box
// $grid->setSelect("Country", "SELECT DISTINCT CountryID, CountryName FROM countries ORDER BY CountryID", true, true, false);
// $grid->setSelect("City", "SELECT DISTINCT CountryID, City FROM countries ORDER BY CountryID", true, true, false);
// depend list box -2
$grid->setColProperty('City', array("edittype" => "select", "editoptions" => array("value" => " :Select")));
$grid->setSelect("Country", "SELECT DISTINCT Country, Country FROM customers ORDER BY Country", true, true, false);
$grid->setSelect("City", "SELECT DISTINCT City, City FROM customers ORDER BY City", true, true, false);
// copas jquery dari source code depend-list

// This event check if we are in add or edit mode and is lunched
// every time the form opens.
// The purpose of thris event is to get the value of the selected
// country and to get the values of the cityes for that country when the form is open.
// Another action here is to select the city from the grid
$beforeshow = <<< BEFORESHOW
function(formid)
{
    // get the value of the country
    var cntryval = $("#Country",formid).val();
    jQuery("#City",formid).html("<option>Select</option>");
    if(cntryval) {
        // if the value is found try to use ajax call to obtain the citys
        // please look at file file city.php
        jQuery.ajax({
            url: 'city.php',
            dataType: 'json',
            data: {q:cntryval},
            success : function(response)
            {
                var t="", sv="";
                var sr = jQuery("#grid").jqGrid('getGridParam','selrow');
                if(sr)
                {
                    // get the selected city from grid
                    sv = jQuery("#grid").jqGrid('getCell',sr,'City');
                    jQuery.each(response, function(i,item) {
                        t += "<option value='"+item.id+"'>"+item.value+"</option>";
                    });
                    // empty the select and put the new selection
                    jQuery("#City",formid).html("").append(t);
                    if(sv)
                    {
                        // select the city value
                        jQuery("#City",formid).val(sv).removeAttr("disabled");
                    }
                }
            }
        });
    } else {
        jQuery("#City",formid).attr("disabled","disabled");
    }
}
BEFORESHOW;

// With this event we bind a change event for the first select.
// If a new value is selected we make ajax call and try to get the citys for this country
// Note that this event should be executed only once if the form is constructed.

$initform = <<< INITFORM
function(formid)
{
    jQuery("#Country",formid).change(function(e) {
        var cntryval = $(this).val();
        if(cntryval) {
            jQuery("#City",formid).html("");
            jQuery.ajax({
                url: 'city.php',
                dataType: 'json',
                data: {q:cntryval},
                success : function(response)
                {
                    var t="";
                    jQuery.each(response, function(i,item) {
                        t += "<option value='"+item.id+"'>"+item.value+"</option>";
                    });
                    jQuery("#City",formid).append(t).removeAttr("disabled");
                }
            });
        }
    });
}
INITFORM;


// Enable navigator
$grid->navigator = true;
$grid->toolbarfilter = true;
// Enable only deleting
// export ke excel
// excel bisa di true untuk mengecek di excel
$grid->setNavOptions('navigator', array("excel" => true, "add" => true, "edit" => true, "del" => true, "view" => true, "search" => false));
// Enjoy

$grid->setNavOptions('add', array("width" => "400", "autodataheight" => true, "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "autodataheight" => true, "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "autodataheight" => true, "closeAfterEdit" => true));

$grid->setNavEvent('edit', 'beforeShowForm', $beforeshow);
$grid->setNavEvent('add', 'beforeShowForm', $beforeshow);
// Bind the initialize Form event in add and edit mode.
$grid->setNavEvent('edit', 'onInitializeForm', $initform);
$grid->setNavEvent('add', 'onInitializeForm', $initform);

$grid->renderGrid('#grid', '#pager', true, null, null, true, true);
$conn = null;
