<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";

// LATIHAN
// Menambahkan autocomplete
require_once ABSPATH . "php/jqAutocomplete.php";

// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");


// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT MemberID id, CardID, LastName, FirstName, BirthDate, Address, Provinsi, Kabupaten, AdmisionFee FROM member';

// menampilkan query tertentu
$grid->ExportCommand = 'SELECT
`mpro`.`Provinsi`
, `mkab`.`Kab_Kota`
, `member`.`LastName`
, `member`.`FirstName`
, `member`.`Sex`
, `member`.`Title`
FROM
`northwind`.`member`
INNER JOIN `northwind`.`mkab` 
    ON (`member`.`Kabupaten` = `mkab`.`id`)
INNER JOIN `northwind`.`mpro` 
    ON (`member`.`Provinsi` = `mpro`.`id_provinsi`)';

// LATIHAN 2
// menampilkan query tertenty2


// Set the table to where you add the data
$grid->table = 'member';
$grid->setPrimaryKeyId('MemberID');
$grid->serialKey = false;
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
// LATIHAN
// hidden untuk menyembunyikan field tersebut dari form
$Model = array(
    // MemberID di sini di hidden
    // selain yang dihidden akan ditampilakn
    array("name" => "MemberID", "label" => "Member ID", "width" => 50, "hidden" => true),
    array("name" => "CardID", "label" => "Card ID", "width" => 25),
    array("name" => "LastName", "label" => "Last Name", "width" => 50),
    array("name" => "FirstName", "label" => "First Name", "width" => 50),
    array("name" => "BirthDate", "label" => "Birth Date", "width" => 35),
    array("name" => "Address", "label" => "Address", "width" => 100),
    array("name" => "Provinsi", "label" => "Provinsi", "width" => 75),
    array("name" => "Kabupaten", "label" => "Kabupaten", "width" => 100, "hidden" => true),
    array("name" => "PostalCode", "label" => "PostalCode", "width" => 25, "hidden" => true),
    array("name" => "HomePhone", "label" => "HomePhone", "width" => 25),
    // currency untuk keuangan
    array(
        "name" => "AdmisionFee", "width" => 80, "align" => "right",
        "formatter" => "currency",
        "formatoptions" => array("decimalPlaces" => 1, "thousandsSeparator" => ",", "prefix" => "Rp ", "suffix" => ""), "sorttype" => "currency"
    )

);
// Let the grid create the model
$grid->setColModel($Model);
// Set the url from where we obtain the data
$grid->setUrl('provinsi.php');
// Set some grid options
$grid->setGridOptions(array(
    "caption" => "MEMBER",
    "rowNum" => 10,
    "rowList" => array(10, 20, 30, 50, 100),
    "height" => 'auto',
    "hoverrows" => true,
    "autowidth" => true,
    "rownumbers" => true,
    "sortname" => "id",
    "sortorder" => "asc",
    "altRows" => true,
    "altclass" => 'myAltRowClass'

));
// LATIHAN2
// memberikan row untuk menambahkan details
$grid->setSubGridGrid('details.php');

// The primary key should be entered
$grid->setDbDate('Y-m-d');
$grid->setDbTime('Y-m-d H:i:s');
$grid->setUserDate('Y-m-d');
$grid->setUserTime('Y-m-d');
// rules
$grid->setColProperty('MemberID', array("editrules" => array("required" => false)));
$grid->setColProperty(
    'BirthDate',
    array(
        "formoptions" => array("label" => "BirthDate"), "editable" => true, "formatter" => "date", "datefmt" => 'd/m/Y', "formatoptions" => array("srcformat" => "Y-m-d H:i:s", "newformat" => "Y-m-d"),
        "editrules" => array("edithidden" => true, "required" => true, "readonly" => false),
        "editoptions" => array("tabindex" => 7, "required" => true, "readonly" => false, "dataInit" =>
        "js:function(elm){setTimeout(function(){
                          jQuery(elm).datepicker({dateFormat:'yy-m-d'});
                             jQuery('.ui-datepicker').css({'zIndex':'1200','font-size':'75%'});},100);}")
    )
);

$grid->setColProperty('Kabupaten', array("editrules" => array("required" => false, "edithidden" => true)));

// LATIHAN
// set autocomplete. Serch for name and ID, but select a ID
// set it only for editing and not on serch
// $grid->setAutocomplete("Country", false, "SELECT DISTINCT Country FROM customers WHERE Country LIKE ? ORDER BY Country", null, true, false);
// ======================================================================================================================
// depent list box
// $grid->setSelect("Country", "SELECT DISTINCT CountryID, CountryName FROM countries ORDER BY CountryID", true, true, false);
// $grid->setSelect("City", "SELECT DISTINCT CountryID, City FROM countries ORDER BY CountryID", true, true, false);
// depend list box -2
$grid->setColProperty('Kabupaten', array("edittype" => "select", "editoptions" => array("value" => " :Select")));
$grid->setSelect("Provinsi", "SELECT DISTINCT id_provinsi, Provinsi FROM mpro ORDER BY Provinsi", true, true, false);
$grid->setSelect("Kabupaten", "SELECT DISTINCT id, Kab_Kota FROM mkab ORDER BY Kab_Kota", true, true, false);

// memunculkan text-area pada form
$grid->setColProperty('Address', array("edittype" => "textarea", "editoptions" => array("rows" => 3, "cols" => 31)));

// copas jquery dari source code depend-list

// This event check if we are in add or edit mode and is lunched
// every time the form opens.
// The purpose of thris event is to get the value of the selected
// provinsi and to get the values of the kabupaten for that provinsi when the form is open.
// Another action here is to select the kabupaten from the grid
$beforeshow = <<< BEFORESHOW
function(formid)
{
    // get the value of the country
    var cntryval = $("#Provinsi",formid).val();
    jQuery("#Kabupaten",formid).html("<option>Select</option>");
    if(cntryval) {
        // if the value is found try to use ajax call to obtain the citys
        // please look at file file city.php
        jQuery.ajax({
            url: 'kabupaten.php',
            dataType: 'json',
            data: {q:cntryval},
            success : function(response)
            {
                var t="", sv="";
                var sr = jQuery("#grid").jqGrid('getGridParam','selrow');
                if(sr)
                {
                    // get the selected city from grid
                    sv = jQuery("#grid").jqGrid('getCell',sr,'Kabupaten');
                    jQuery.each(response, function(i,item) {
                        t += "<option value='"+item.id+"'>"+item.value+"</option>";
                    });
                    // empty the select and put the new selection
                    jQuery("#Kabupaten",formid).html("").append(t);
                    if(sv)
                    {
                        // select the city value
                        jQuery("#Kabupaten",formid).val(sv).removeAttr("disabled");
                    }
                }
            }
        });
    } else {
        jQuery("#Kabupaten",formid).attr("disabled","disabled");
    }
}
BEFORESHOW;

// With this event we bind a change event for the first select.
// If a new value is selected we make ajax call and try to get the citys for this country
// Note that this event should be executed only once if the form is constructed.

$initform = <<< INITFORM
function(formid)
{
    jQuery("#Provinsi",formid).change(function(e) {
        var cntryval = $(this).val();
        if(cntryval) {
            jQuery("#Kabupaten",formid).html("");
            jQuery.ajax({
                url: 'kabupaten.php',
                dataType: 'json',
                data: {q:cntryval},
                success : function(response)
                {
                    var t="";
                    jQuery.each(response, function(i,item) {
                        t += "<option value='"+item.id+"'>"+item.value+"</option>";
                    });
                    jQuery("#Kabupaten",formid).append(t).removeAttr("disabled");
                }
            });
        }
    });
}
INITFORM;


// Enable navigator
$grid->navigator = true;
$grid->toolbarfilter = true;
// Enable only deleting
// export ke excel
// excel bisa di true untuk mengecek di excel
$grid->setNavOptions('navigator', array("excel" => true, "pdf" => true, "add" => true, "edit" => true, "del" => true, "view" => true, "search" => true));
// Enjoy

// setelah di simpan otomatis menutup
$grid->setNavOptions('add', array("width" => "400", "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "closeAfterEdit" => true));

$grid->setNavEvent('edit', 'beforeShowForm', $beforeshow);
$grid->setNavEvent('add', 'beforeShowForm', $beforeshow);
// Bind the initialize Form event in add and edit mode.
$grid->setNavEvent('edit', 'onInitializeForm', $initform);
$grid->setNavEvent('add', 'onInitializeForm', $initform);

// LATIHAN 2
$grid->setGridOptions(array("footerrow" => true, "userDataOnFooter" => true));
// At end call footerData to put total  label
$grid->callGridMethod('#grid', 'footerData', array("set", array("HomePhone" => "Total:")));
// Set which parameter to be sumarized
$summaryrows = array("AdmisionFee" => array("AdmisionFee" => "SUM"));
$grid->renderGrid('#grid', '#pager', true, $summaryrows, null, true, true);
$conn = null;
