<?php
require_once '../../../jq-config.php';
require_once ABSPATH . "php/jqGridPdo.php";
$prov = $_GET['q'];
if ($prov) {
	try {
		$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
		$SQL =  "SELECT DISTINCT(id) id, Kab_Kota value FROM mkab WHERE id_provinsi='" . $prov . "' ORDER BY Kab_Kota";
		$collation = jqGridDB::query($conn, "SET NAMES utf8");
		$kab = jqGridDB::query($conn, $SQL);
		$result = jqGridDB::fetch_object($kab, true, $conn);
		echo json_encode($result);
	} catch (Exception $e) {
		echo $e->getMessage();
	}
}
