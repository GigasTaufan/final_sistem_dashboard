<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT `CompanyName`,`ContactName`,`ContactTitle`,`Address`,`City`,`Country` FROM `customers`';
// set the ouput format to json
$grid->dataType = 'json';
// Let the grid create the model from SQL query
$grid->setColModel();
// Set the url from where we obtain the data
$grid->setUrl('grouping.php');
// Set alternate background using altRows property
$grid->setGridOptions(array(
    "caption" => "GROUPING",
    "rowNum" => 10,
    "sortname" => "CompanyName",
    "rowList" => array(10, 20, 50),
    "height" => 'auto',
    "autowidth" => true,
    "grouping" => true,
    "altRows" => true,
    "altclass" => 'myAltRowClass',
    "hoverrows" => true,
    "groupingView" => array(
        "groupField" => array('Country'),
        "groupColumnShow" => array(true),
        "groupText" => array('<b>{0}</b>'),
        "groupDataSorted" => true

    )
));
// Change some property of the field(s)
$grid->setColProperty("OrderID", array("label" => "ID", "width" => 60));
$grid->setColProperty(
    "OrderDate",
    array(
        "formatter" => "date",
        "formatoptions" => array("srcformat" => "Y-m-d H:i:s", "newformat" => "m/d/Y")
    )
);
// memeberikan set select
$grid->setSelect("Country", "SELECT DISTINCT Country, Country FROM customers ORDER BY Country", true, true, true, array("" => "All"));

// filter 
$grid->toolbarfilter = true;
// Enjoy
$grid->renderGrid('#grid', '#pager', true, null, null, true, true);
$conn = null;
