<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
jqGridDB::query($conn, "SET NAMES utf8");
$rowid = jqGridUtils::Strip($_REQUEST["rowid"]);
if (!$rowid) die("Missed parameters");
// Get details
$SQL = 'SELECT
`member`.`MemberID`
,`member`.`FirstName`
, `member`.`LastName`
, `member`.`BirthDate`
, `mpro`.`Provinsi`
, `mkab`.`Kab_Kota`
FROM
`northwind`.`member`
LEFT JOIN `northwind`.`mpro` 
    ON (`member`.`Provinsi` = `mpro`.`id_provinsi`)
LEFT JOIN `northwind`.`mkab` 
    ON (`member`.`Kabupaten` = `mkab`.`id`) WHERE MemberID=' . (int)$rowid;


$qres = jqGridDB::query($conn, $SQL);
$result = jqGridDB::fetch_assoc($qres, $conn);
$s = "<table><tbody>";
$s .= "<tr><td><b>First Name: </b></td><td>" . $result["FirstName"] . "</td>";
$s .= "<td rowspan='9' valign='top'><img src='images/" . trim($result["MemberID"]) . ".jpg'/></td></tr>";
$s .= "<tr><td><b>Last Name : </b></td><td>" . $result["LastName"] . "</td></tr>";
$s .= "<tr><td><b>Birth Date : </b></td><td>" . $result["BirthDate"] . "</td></tr>";
$s .= "<tr><td><b>Provinsi : </b></td><td>" . $result["Provinsi"] . "</td></tr>";
$s .= "<tr><td><b>Kabupaten : </b></td><td>" . $result["Kab_Kota"] . "</td></tr>";
$s .= "</tbody></table>";
echo $s;
jqGridDB::closeCursor($qres);
