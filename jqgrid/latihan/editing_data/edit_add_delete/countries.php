<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";

// LATIHAN
// Menambahkan autocomplete
require_once ABSPATH . "php/jqAutocomplete.php";

// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");


// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT CountryID id, CountryName, City FROM countries';
// Set the table to where you add the data
$grid->table = 'countries';
$grid->setPrimaryKeyId('CountryID');
$grid->serialKey = false;
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
// LATIHAN
// hidden untuk menyembunyikan field tersebut dari form
$Model = array(
	// MemberID di sini di hidden
	// selain yang dihidden akan ditampilakn
	array("name" => "CountryID", "label" => "Country ID", "width" => 25, "hidden" => true),
	array("name" => "CountryName", "label" => "CountryName", "width" => 25),
	array("name" => "City", "label" => "City", "width" => 25)

);
// Let the grid create the model
$grid->setColModel($Model);
// Set the url from where we obtain the data
$grid->setUrl('countries.php');
// Set some grid options
$grid->setGridOptions(array(
	"caption" => "Countries",
	"rowNum" => 10,
	"rowList" => array(10, 20, 30, 50, 100),
	"height" => 'auto',
	"hoverrows" => true,
	"autowidth" => true,
	"rownumbers" => true,
	"sortname" => "CountryID",
	"sortorder" => "asc",
	"altRows" => true,
	"altclass" => 'myAltRowClass'

));
// The primary key should be entered

// LATIHAN
// set autocomplete. Serch for name and ID, but select a ID
// set it only for editing and not on serch
$grid->setAutocomplete("Country", false, "SELECT DISTINCT Country FROM customers WHERE Country LIKE ? ORDER BY Country", null, true, false);


// Enable navigator
$grid->navigator = true;
$grid->toolbarfilter = true;

// auto menutup setelah input data baru, edit data
$grid->setNavOptions('add', array("width" => "400", "dataheight" => "200", "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "dataheight" => "200", "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "dataheight" => "200", "closeAfterEdit" => true));
// Enable only deleting
// export ke excel
// excel bisa di true untuk mengecek di excel
$grid->setNavOptions('navigator', array("excel" => true, "add" => true, "edit" => true, "del" => true, "view" => false, "search" => false));
// Enjoy
$grid->renderGrid('#grid', '#pager', true, null, null, true, true);
$conn = null;
