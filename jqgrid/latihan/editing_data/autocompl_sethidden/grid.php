<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// include the autocomplete class
require_once ABSPATH . "php/jqAutocomplete.php";

// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");
// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT
                    `suppliers`.`SupplierID`
                    ,`products`.`ProductID`
                    , `suppliers`.`CompanyName`
                    , `products`.`ProductName`
                    , `products`.`UnitPrice`
                    FROM
                    `northwind`.`products`
                    INNER JOIN `northwind`.`suppliers` 
                        ON (`products`.`SupplierID` = `suppliers`.`SupplierID`)';
// set the ouput format to json
$grid->dataType = 'json';
$grid->table = "products";
$grid->setPrimaryKeyId("ProductID");
// Let the grid create the model
$grid->setColModel();
// Set the url from where we obtain the data
$grid->setUrl('grid.php');
// Set grid caption using the option caption
$grid->setGridOptions(
    array(
        "caption" => "Advanced Autocomplete",
        "rowNum" => 10,
        "sortname" => "ProductID",
        "hoverrows" => true,
        "rowList" => array(10, 50, 100),
        "height" => "auto",
        "autowidth" => true
    )

);
// Change some property of the field(s)
$grid->setColProperty("ProductID", array("label" => "ID", "width" => 60, "hidden" => true));
$grid->setColProperty("SupplierID", array("editoptions" => array("readonly" => "readonly"), "align" => "center"));

// set autocomplete. Serch for name and ID, but select a ID
// set it only for editing and not on serch

// mencari 
$grid->setAutocomplete("CompanyName", "#SupplierID", "SELECT CompanyName, CompanyName,SupplierID FROM suppliers WHERE CompanyName LIKE ? ORDER BY CompanyName", null, true, false);
// Enjoy

$grid->setNavOptions('add', array("width" => "400", "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "closeAfterEdit" => true));

$grid->navigator = true;
$grid->setNavOptions('navigator', array("search" => false, "excel" => false));
$grid->setNavOptions('edit', array("height" => 'auto', "dataheight" => 'auto'));
$grid->renderGrid('#grid', '#pager', true, null, null, true, true);
$conn = null;
