<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");
// Get the needed parameters passed from the main grid
$rowid = jqGridUtils::GetParam('OrderID', 0);
// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = "SELECT
							`order_details`.OrderDetailsID
							, `order_details`.`ProductID`
							, `order_details`.`OrderID`
							, `order_details`.`UnitPrice`
							, `order_details`.`Quantity`
							, `order_details`.`Discount`
						FROM
							`order_details`							
						WHERE OrderID = '" . $rowid . "'";

// Set the table to where you update the data
$grid->table = 'order_details';
// set the ouput format to json
$grid->dataType = 'json';
// Let the grid create the model
$grid->setPrimaryKeyId('OrderDetailsID');
$grid->setColModel(null, array((int)$rowid));
// Set the url from where we obtain the data
$grid->setUrl('subdetail.php');
// Set some grid options
$grid->setGridOptions(array(
	"caption" => "ORDERS",
	"rowNum" => 10,
	"rowList" => array(10, 20, 30, 50, 100),
	"height" => 'auto',
	"hoverrows" => true,
	"autowidth" => true,
	"rownumbers" => true,
	"sortname" => "OrderID",
	"sortorder" => "asc",
	"altRows" => true,
	"altclass" => 'myAltRowClass',


));

$grid->setGridOptions(array("footerrow" => true, "userDataOnFooter" => true));

// Change some property of the field(s)
$grid->setSelect("ProductID", "SELECT DISTINCT ProductID, ProductName FROM products ORDER BY ProductName", true, true, false);

// on beforeshow form when add we get the order id and set it for posting
$beforeshow = <<<BEFORE
function(formid)
{
var srow = jQuery("#detail").jqGrid('getGridParam','selrow');
if(srow) {
	var gridrow = jQuery("#detail").jqGrid('getRowData',srow);
	$("#OrderID",formid).val(gridrow.OrderID);
}
}
BEFORE;

// disable the CRUD buttons when we initialy load the grid
$initgrid = <<< INIT
jQuery("#add_subdetail").addClass("ui-state-disabled");
jQuery("#edit_subdetail").addClass("ui-state-disabled");
jQuery("#del_subdetail").addClass("ui-state-disabled");
INIT;


$grid->setJSCode($initgrid);
$grid->setColProperty("OrderDetailsID", array("hidden" => true, "width" => 20));

$grid->navigator = true;
$grid->toolbarfilter = true;
$grid->setNavOptions('navigator', array("excel" => true, "add" => true, "edit" => true, "del" => true, "view" => false));
$grid->setNavEvent('add', 'beforeShowForm', $beforeshow);
// Enjoy

$grid->renderGrid("#subdetail", "#subpgdetail", true, null, array((int)$rowid), true, true);
$conn = null;
