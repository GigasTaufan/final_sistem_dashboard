<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");
// Get the needed parameters passed from the main grid
$rowid = jqGridUtils::GetParam('CustomerID', 0);
// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = "SELECT
						`OrderID`
						, `CustomerID`
						, `EmployeeID`
						, `ShipCity`
						, `ShipPostalCode`
						, `ShipCountry`
						FROM
						`northwind`.`orders` WHERE CustomerID= '" . $rowid . "'";
// set the ouput format to json
$grid->dataType = 'json';
// table untuk melihat update
$grid->table = 'orders';
// Let the grid create the model
$grid->setPrimaryKeyId('OrderID');
$grid->setColModel(null, array((int)$rowid));
// Set the url from where we obtain the data
$grid->setUrl('detail.php');
// Set some grid options
$grid->setGridOptions(array(
	"caption" => "ORDER",
	"rowNum" => 10,
	"rowList" => array(10, 20, 30, 50, 100),
	"height" => 'auto',
	"hoverrows" => true,
	"autowidth" => true,
	"rownumbers" => true,
	"sortname" => "CustomerID",
	"sortorder" => "asc",
	"altRows" => true,
	"altclass" => 'myAltRowClass',


));
// Change some property of the field(s)

// on beforeshow form when add we get the customer id and set it for posting
$beforeshow = <<<BEFORE
function(formid)
{
var srow = jQuery("#grid").jqGrid('getGridParam','selrow');
if(srow) {
	var gridrow = jQuery("#grid").jqGrid('getRowData',srow);
	$("#CustomerID",formid).val(gridrow.CustomerID);
}
}
BEFORE;

// disable the CRUD buttons when we initialy load the grid
$initgrid = <<< INIT
jQuery("#add_detail").addClass("ui-state-disabled");
jQuery("#edit_detail").addClass("ui-state-disabled");
jQuery("#del_detail").addClass("ui-state-disabled");
INIT;

$selectorder = <<<ORDER
function(rowid, selected)
{
    if(rowid != null) {
		
        jQuery("#subdetail").jqGrid('setGridParam',{postData:{OrderID:rowid}});
        jQuery("#subdetail").trigger("reloadGrid");
		// Enable CRUD buttons in navigator when a row is selected
		jQuery("#add_subdetail").removeClass("ui-state-disabled");
		jQuery("#edit_subdetail").removeClass("ui-state-disabled");
		jQuery("#del_subdetail").removeClass("ui-state-disabled");
    }
}
ORDER;
// We should clear the grid data on second grid on sorting, paging, etc.
$cleargrid = <<<CLEAR
function(rowid, selected)
{
   // clear the grid data and footer data
	jQuery("#subdetail").jqGrid('clearGridData',true);
	// Disable CRUD buttons in navigator when a row is not selected
	jQuery("#add_subdetail").addClass("ui-state-disabled");
	jQuery("#edit_subdetail").addClass("ui-state-disabled");
	jQuery("#del_subdetail").addClass("ui-state-disabled");
}
CLEAR;

$grid->setGridEvent('onSelectRow', $selectorder);
$grid->setGridEvent('onSortCol', $cleargrid);
$grid->setGridEvent('onPaging', $cleargrid);


$grid->setJSCode($initgrid);
$grid->setColProperty("CustomerID", array("hidden" => true, "width" => 20));
$grid->setColProperty("OrderID", array("hidden" => false, "width" => 20));
$grid->setColProperty("EmployeeID", array("hidden" => true, "width" => 20));
$grid->navigator = true;
$grid->toolbarfilter = true;

$grid->setNavOptions('add', array("width" => "400", "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "closeAfterEdit" => true));

$grid->setNavOptions('navigator', array("excel" => true, "add" => true, "edit" => true, "del" => true, "view" => false));
$grid->setNavEvent('add', 'beforeShowForm', $beforeshow);
// Enjoy
$grid->renderGrid("#detail", "#pgdetail", true, null, array((int)$rowid), true, true);
$conn = null;
