<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
// Suplier
$grid->SelectCommand = 'SELECT
                        `CustomerID`
                        , `CompanyName`
                        , `ContactName`
                        , `ContactTitle`
                        , `Address`
                        , `City`
                        , `Country`
                        FROM
                        `northwind`.`customers`';
// Set the table to where you update the data
$grid->table = 'customers';
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
$grid->setColModel();
// Set the url from where we obtain the data
$grid->setUrl('grid.php');
// Set some grid options
$grid->setGridOptions(array(
    "caption" => "CUSTOMERS",
    "rowNum" => 10,
    "rowList" => array(10, 20, 30, 50, 100),
    "height" => 'auto',
    "hoverrows" => true,
    "autowidth" => true,
    "rownumbers" => true,
    "sortname" => "CustomerID",
    "sortorder" => "asc",
    "altRows" => true,
    "altclass" => 'myAltRowClass',


));
$grid->setColProperty('CustomerID', array("label" => "ID", "hidden" => true, "width" => 50));
// formnya menjadi dua kolom
// $grid->setColProperty('Address', array("formoptions" => array("rowpos" => 2, "colpos" => 2)));
// $grid->setColProperty('City', array("formoptions" => array("rowpos" => 4, "colpos" => 2)));
// $grid->setColProperty('Country', array("formoptions" => array("rowpos" => 3, "colpos" => 2)));

$grid->callGridMethod("#grid", "setGroupHeaders", array(array(
    "useColSpanStyle" => true,
    "groupHeaders" => array(
        array(
            "startColumnName" => 'Address',
            "numberOfColumns" => 3,
            "titleText" => 'Location'
        )
    )
)));

$selectorder = <<<ORDER
function(rowid, selected)
{
    if(rowid != null) {
		
        jQuery("#detail").jqGrid('setGridParam',{postData:{CustomerID:rowid}});
        jQuery("#detail").trigger("reloadGrid");
		// Enable CRUD buttons in navigator when a row is selected
		jQuery("#add_detail").removeClass("ui-state-disabled");
		jQuery("#edit_detail").removeClass("ui-state-disabled");
		jQuery("#del_detail").removeClass("ui-state-disabled");
    }
}
ORDER;
// We should clear the grid data on second grid on sorting, paging, etc.
$cleargrid = <<<CLEAR
function(rowid, selected)
{
   // clear the grid data and footer data
	jQuery("#detail").jqGrid('clearGridData',true);
	// Disable CRUD buttons in navigator when a row is not selected
	jQuery("#add_detail").addClass("ui-state-disabled");
	jQuery("#edit_detail").addClass("ui-state-disabled");
	jQuery("#del_detail").addClass("ui-state-disabled");
}
CLEAR;
// =======================================================================================
// dropdown select
$grid->setColProperty('City', array("edittype" => "select", "editoptions" => array("value" => " :Select")));
$grid->setSelect("Country", "SELECT DISTINCT Country, Country FROM customers ORDER BY Country", true, true, false);
$grid->setSelect("City", "SELECT DISTINCT City, City FROM customers ORDER BY City", true, true, false);


// This event check if we are in add or edit mode and is lunched
// every time the form opens.
// The purpose of thris event is to get the value of the selected
// provinsi and to get the values of the kabupaten for that provinsi when the form is open.
// Another action here is to select the kabupaten from the grid
$beforeshow = <<< BEFORESHOW
function(formid)
{
    // get the value of the country
    var cntryval = $("#Country",formid).val();
    jQuery("#City",formid).html("<option>Select</option>");
    if(cntryval) {
        // if the value is found try to use ajax call to obtain the citys
        // please look at file file city.php
        jQuery.ajax({
            url: 'City.php',
            dataType: 'json',
            data: {q:cntryval},
            success : function(response)
            {
                var t="", sv="";
                var sr = jQuery("#grid").jqGrid('getGridParam','selrow');
                if(sr)
                {
                    // get the selected city from grid
                    sv = jQuery("#grid").jqGrid('getCell',sr,'City');
                    jQuery.each(response, function(i,item) {
                        t += "<option value='"+item.id+"'>"+item.value+"</option>";
                    });
                    // empty the select and put the new selection
                    jQuery("#City",formid).html("").append(t);
                    if(sv)
                    {
                        // select the city value
                        jQuery("#City",formid).val(sv).removeAttr("disabled");
                    }
                }
            }
        });
    } else {
        jQuery("#City",formid).attr("disabled","disabled");
    }
}
BEFORESHOW;

// With this event we bind a change event for the first select.
// If a new value is selected we make ajax call and try to get the citys for this country
// Note that this event should be executed only once if the form is constructed.

$initform = <<< INITFORM
function(formid)
{
    jQuery("#Country",formid).change(function(e) {
        var cntryval = $(this).val();
        if(cntryval) {
            jQuery("#City",formid).html("");
            jQuery.ajax({
                url: 'City.php',
                dataType: 'json',
                data: {q:cntryval},
                success : function(response)
                {
                    var t="";
                    jQuery.each(response, function(i,item) {
                        t += "<option value='"+item.id+"'>"+item.value+"</option>";
                    });
                    jQuery("#City",formid).append(t).removeAttr("disabled");
                }
            });
        }
    });
}
INITFORM;

$grid->setNavEvent('edit', 'beforeShowForm', $beforeshow);
$grid->setNavEvent('add', 'beforeShowForm', $beforeshow);
// Bind the initialize Form event in add and edit mode.
$grid->setNavEvent('edit', 'onInitializeForm', $initform);
$grid->setNavEvent('add', 'onInitializeForm', $initform);
// ==================================================================================================

$grid->setGridEvent('onSelectRow', $selectorder);
$grid->setGridEvent('onSortCol', $cleargrid);
$grid->setGridEvent('onPaging', $cleargrid);

$grid->setNavOptions('add', array("width" => "400", "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "closeAfterEdit" => true));
// Enable navigator
$grid->navigator = true;
$grid->toolbarfilter = true;
// Enable only editing
$grid->setNavOptions('navigator', array("excel" => false, "add" => true, "edit" => true, "del" => true, "view" => true));
// Enjoy
$grid->renderGrid('#grid', '#pager', true, null, null, true, true);
$conn = null;
