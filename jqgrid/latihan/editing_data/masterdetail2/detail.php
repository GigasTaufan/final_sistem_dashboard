<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");
// Get the needed parameters passed from the main grid
$rowid = jqGridUtils::GetParam('SupplierID', 0);
// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = "SELECT
						`ProductID`
						, `ProductName`
						, `SupplierID`
						, `QuantityPerUnit`
						, `UnitPrice`
						, `UnitsInStock`
						FROM
						`northwind`.`products` WHERE SupplierID= ?";
// set the ouput format to json
$grid->dataType = 'json';
// table untuk melihat update
$grid->table = 'products';
// Let the grid create the model
$grid->setPrimaryKeyId('ProductID');
$grid->setColModel(null, array((int)$rowid));
// Set the url from where we obtain the data
$grid->setUrl('detail.php');
// Set some grid options
$grid->setGridOptions(array(
	"caption" => "Product Details",
	"rowNum" => 10,
	"rowList" => array(10, 20, 30, 50, 100),
	"height" => 'auto',
	"hoverrows" => true,
	"autowidth" => true,
	"rownumbers" => true,
	"sortname" => "SupplierID",
	"sortorder" => "asc",
	"altRows" => true,
	"altclass" => 'myAltRowClass',


));
// Change some property of the field(s)

// on beforeshow form when add we get the customer id and set it for posting
$beforeshow = <<<BEFORE
function(formid)
{
var srow = jQuery("#grid").jqGrid('getGridParam','selrow');
if(srow) {
	var gridrow = jQuery("#grid").jqGrid('getRowData',srow);
	$("#SupplierID",formid).val(gridrow.SupplierID);
}
}
BEFORE;

// disable the CRUD buttons when we initialy load the grid
$initgrid = <<< INIT
jQuery("#add_detail").addClass("ui-state-disabled");
jQuery("#edit_detail").addClass("ui-state-disabled");
jQuery("#del_detail").addClass("ui-state-disabled");
INIT;


$grid->setJSCode($initgrid);
$grid->setColProperty("SupplierID", array("hidden" => true, "width" => 20));
$grid->setColProperty("ProductID", array("hidden" => true, "width" => 20));
$grid->navigator = true;
$grid->toolbarfilter = true;
$grid->setNavOptions('navigator', array("excel" => true, "add" => true, "edit" => true, "del" => true, "view" => false));
$grid->setNavEvent('add', 'beforeShowForm', $beforeshow);
// Enjoy
$grid->renderGrid("#detail", "#pgdetail", true, null, array((int)$rowid), true, true);
$conn = null;
