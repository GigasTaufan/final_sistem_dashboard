<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH."php/jqGrid.php";
// include the driver class
require_once ABSPATH."php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT EmployeeID, FirstName, LastName, BirthDate FROM employees';
// Set the table to where you add the data
$grid->table = 'employees';
$grid->setPrimaryKeyId('EmployeeID');
$grid->serialKey = false;
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
$grid->setColModel();
// Set the url from where we obtain the data
$grid->setUrl('grid.php');
// Set some grid options
$grid->setGridOptions(array(
    "caption"=>"Employees",
    "rowNum"=>10,    
    "rowList"=>array(10,20,30,50,100),
	"height"=>'auto',
	"hoverrows"=>true,
	"width"=>'1030',
	"rownumbers"=>true,
	"sortname"=>"EmployeeID",
	"sortorder"=>"asc",
	"altRows"=>true,
	"altclass"=>'myAltRowClass'
	
));
// The primary key should be entered

$grid->setDbDate('Y-m-d');
$grid->setDbTime('Y-m-d H:i:s'); 
$grid->setUserDate('d/m/Y');
// the same as formatter
$grid->setUserTime('d/m/Y');

$grid->setColProperty('EmployeeID', array("editrules"=>array("required"=>true)));

$grid->setColProperty('BirthDate', 
             array("formoptions"=>array("label"=>"BirthDate"),"editable"=>true,"formatter"=>"date","datefmt"=>'d/m/Y',"formatoptions"=>array("srcformat"=>"Y-m-d H:i:s", "newformat"=>"d/m/Y"),
				   "editrules"=>array("edithidden"=>true,"required"=>true,"readonly"=>false),
                   "editoptions"=>array("tabindex"=>7,"required"=>true,"readonly"=>false,"dataInit"=>
                      "js:function(elm){setTimeout(function(){
                          jQuery(elm).datepicker({dateFormat:'dd/mm/yy'});
                             jQuery('.ui-datepicker').css({'zIndex':'1200','font-size':'75%'});},100);}")
            ));
			
// Enable navigator
$grid->navigator = true;

$grid->setNavOptions('add',array("width"=>"400","dataheight"=>"200","closeAfterAdd"=>true));
$grid->setNavOptions('view',array("width"=>"400","dataheight"=>"200","datawidth"=>"auto"));
$grid->setNavOptions('edit',array("width"=>"400","dataheight"=>"200","closeAfterEdit"=>true));

// Enable only deleting
$grid->setNavOptions('navigator', array("excel"=>false,"add"=>true,"edit"=>true,"del"=>true,"view"=>false, "search"=>false)); 
// Enjoy
$grid->renderGrid('#grid','#pager',true, null, null, true,true); 
$conn = null;
?>
