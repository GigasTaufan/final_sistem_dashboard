<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH."php/jqGrid.php";
// include the driver class
require_once ABSPATH."php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT EmployeeID, FirstName, LastName, BirthDate FROM employees';
// Set the table to where you add the data
$grid->table = 'employees';
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
$grid->setColModel();
// Set the url from where we obtain the data
$grid->setUrl('grid.php');
// add a action column and instruct the formatter to create the needed butons
// for inline editing
$grid->addCol(array(
    "name"=>"actions",
    "formatter"=>"actions",
    "editable"=>false,
    "sortable"=>false,
    "resizable"=>false,
    "fixed"=>true,
    "width"=>60,
    // use keys to save or cancel a row.
    "formatoptions"=>array("keys"=>true)
    ), "first");
	
$grid->setDbDate('Y-m-d');
$grid->setDbTime('Y-m-d H:i:s'); 
$grid->setUserDate('d/m/Y');
// the same as formatter
$grid->setUserTime('d/m/Y');
	
$grid->setColProperty('EmployeeID', array("editable"=>false, "width"=>50, "label"=>"ID"));
$grid->setColProperty('BirthDate', 
             array("formoptions"=>array("label"=>"BirthDate"),"editable"=>true,"formatter"=>"date","datefmt"=>'d/m/Y',"formatoptions"=>array("srcformat"=>"Y-m-d", "newformat"=>"d/m/Y"),
				   "editrules"=>array("edithidden"=>true,"required"=>true,"readonly"=>false),
                   "editoptions"=>array("required"=>true,"readonly"=>false,"dataInit"=>
                      "js:function(elm){setTimeout(function(){
                          jQuery(elm).datepicker({dateFormat:'dd/mm/yy'});
                             jQuery('.ui-datepicker').css({'zIndex':'1200','font-size':'75%'});},100);}")
            ));
// Set some grid options
$grid->setGridOptions(array(
    "caption"=>"Employee",
    "rowNum"=>10,    
    "rowList"=>array(10,20,30,50,100),
	"height"=>'auto',
	"hoverrows"=>true,
	"width"=>'1030',
	"rownumbers"=>true,
	"sortname"=>"EmployeeID",
	"sortorder"=>"asc",
	"altRows"=>true,
	"altclass"=>'myAltRowClass'
	
));
// Enjoy
$grid->renderGrid('#grid','#pager',true, null, null, true,true);
$conn = null;
?>
