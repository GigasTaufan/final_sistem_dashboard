<?php 
require_once '../../../tabs.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>jqGrid PHP Demo</title>
    <link rel="stylesheet" type="text/css" media="screen" href="../../../themes/redmond/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../../themes/ui.multiselect.css" />
         
    <script src="../../../js/jquery-1.7.min.js" type="text/javascript"></script>    
    <script src="../../../js/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script>
	$(function() {
	$( "#tabs" ).tabs();
	});
	
	</script>
    <script type="text/javascript">
    $.jgrid.no_legacy_api = true;
    $.jgrid.useJSON = true;
	$(document).ready(function() {
	
	});			
    </script>
    
    <script type="text/ecmascript" src="../../../js/jquery.jqGrid.min.js"></script>
    <script type="text/ecmascript" src="../../../js/trirand/jquery.jqGrid.min.js"></script>    
    <script src="../../../js/jquery-ui-custom.min.js" type="text/javascript"></script>
    
    <link rel="stylesheet" type="text/css" href="../../../themes/bootstrap/css/bootstrap.min.css"/>    
    <link rel="stylesheet" type="text/css" media="screen" href="../../../themes/trirand/ui.jqgrid-bootstrap.css" />
    
    <script>
        $.jgrid.defaults.width = 780;
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';

    </script>
  
    
	<style type="text/css">
        .myAltRowClass { background-color: #DDDDDC; background-image: none; }
        /*.myAltRowClass { background: #DDDDDC; }*/
    </style>  
	

	 <link rel="stylesheet" type="text/css" href="../../css/redmond/jquery-ui.css">  
  </head>
  <body>
      <div>
          <?php include ("grid.php");?>
      </div>
      <br/>
      <?php tabs(array("grid.php"));?>
   </body>
</html>
