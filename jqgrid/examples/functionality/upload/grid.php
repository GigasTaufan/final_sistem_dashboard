<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH."php/jqGrid.php";
// include the driver class
require_once ABSPATH."php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN,DB_USER,DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT CustomerID, CompanyName, Phone, PostalCode, City FROM customers';
// Set the table to where you add the data
$grid->table = 'customers';

// Set output format to json
$grid->dataType = 'json';
$grid->serialKey = false;
// Let the grid create the model
$grid->setColModel();
// Set the url from where we obtain the data
$grid->setUrl('grid.php');
// Set some grid options
$grid->setGridOptions(array(
    "rowNum"=>10,
    "rowList"=>array(10,20,30),
    "sortname"=>"CustomerID"
));
// The primary key should be entered
$grid->setColProperty('CustomerID', array("editrules"=>array("required"=>true)));

//Create the column for the file upload. Hide it, but enable it when editiing
// Note - the name (in this case this is the id of the created form) is important.
// See the code for upload options fileElementId:'fileToUpload'
// the same name should be present in doajaxfileupload.php - see $fileElementName
$grid->addCol(array("name"=>"fileToUpload", "hidden"=>true, "editable"=>true, "edittype"=>"file", "editrules"=>array("edithidden"=>true)));

// The code for the file upload.
// This code will be placed in onIntializeForm event

$upload = <<<UPLOAD
function(formid) {
//These are needed for fileupload plugin
$(formid).attr("method","POST");
$(formid).attr("action","");
$(formid).attr("enctype","multipart/form-data");
// Create a button bellow the file field
$("<br/><button id='buttonUpload'>Upload</button>").insertAfter("#fileToUpload",formid);
// bind a event
$("#buttonUpload",formid).click(function(){
	$.ajaxFileUpload({
		url:'doajaxfileupload.php',
		secureuri:false,
		fileElementId:'fileToUpload',
		dataType: 'json',
		success: function (data, status) {
			console.log(data);
			if(typeof(data.error) != 'undefined')
			{
				if(data.error != '')
				{
					alert(data.error);
				}else{
					$("#fileToUpload").val("");
					alert(data.msg);
				}
			}
		},
		error: function (data, status, e)
		{
			alert(e);
		}
	});
	return false;
});
}
UPLOAD;
// add the event in add and edit module
$grid->setNavOptions('edit',array("width"=>400, "dataheight"=>200));
$grid->setNavEvent('edit', 'onInitializeForm', $upload);
$grid->setNavOptions('add',array("width"=>400, "dataheight"=>200));
$grid->setNavEvent('add', 'onInitializeForm', $upload);

// Enable navigator
$grid->navigator = true;
// Enable only deleting
$grid->setNavOptions('navigator', array("excel"=>false,"add"=>true,"edit"=>true,"del"=>true,"view"=>false, "search"=>false));
// Enjoy
$grid->renderGrid('#grid','#pager',true, null, null, true,true);
$conn = null;
?>
