<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT idpeminjaman, idmember, idbuku, tglpinjam, tglkembali FROM peminjaman';
// Set the table to where you add the data
$grid->table = 'peminjaman';
$grid->setPrimaryKeyId('idpeminjaman');
$grid->serialKey = false;
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
$Model = array(
	array("name" => "idpeminjaman", "label" => "ID Peminjaman", "autowidth" => true, "hidden" => true),
	array("name" => "idmember", "label" => "Nama Member", "autowidth" => true),
	array("name" => "idbuku", "label" => "Judul Buku", "autowidth" => true),
	array("name" => "tglpinjam", "label" => "Tgl Pinjam", "autowidth" => true),
	array("name" => "tglkembali", "label" => "Tgl Kembali", "autowidth" => true)
);
$grid->setColModel($Model);
// Set the url from where we obtain the data
$grid->setUrl('peminjaman.php');
// Set some grid options
$grid->setGridOptions(array(
	"caption" => "PEMINJAMAN",
	"rowNum" => 10,
	"rowList" => array(10, 20, 30, 50, 100),
	"height" => 'auto',
	"hoverrows" => true,
	"autowidth" => true,
	"rownumbers" => true,
	"sortname" => "idpeminjaman",
	"sortorder" => "asc",
	"altRows" => true,
	"altclass" => 'myAltRowClass'

));
// The primary key should be entered
$grid->setColProperty('idpeminjaman', array("editrules" => array("required" => false)));

// Select untuk Nama Peminjam
$grid->setSelect('idmember', "SELECT idmember, nama FROM anggota");
$grid->setSelect('idbuku', "SELECT idbuku, judul FROM buku");

// Select untuk Judul Buku

$grid->setDbDate('Y-m-d');
$grid->setDbTime('Y-m-d H:i:s');
$grid->setUserDate('Y-m-d');
$grid->setUserTime('Y-m-d');

$grid->setColProperty(
	'tglpinjam',
	array(
		"formoptions" => array("label" => "tglpinjam"), "editable" => true, "formatter" => "date", "datefmt" => 'd/m/Y', "formatoptions" => array("srcformat" => "Y-m-d H:i:s", "newformat" => "Y-m-d"),
		"editrules" => array("edithidden" => true, "required" => true, "readonly" => false),
		"editoptions" => array("tabindex" => 7, "required" => true, "readonly" => false, "dataInit" =>
		"js:function(elm){setTimeout(function(){
                          jQuery(elm).datepicker({dateFormat:'yy-m-d'});
                             jQuery('.ui-datepicker').css({'zIndex':'1200','font-size':'75%'});},100);}")
	)
);

$grid->setColProperty(
	'tglkembali',
	array(
		"formoptions" => array("label" => "tglkembali"), "editable" => true, "formatter" => "date", "datefmt" => 'd/m/Y', "formatoptions" => array("srcformat" => "Y-m-d H:i:s", "newformat" => "Y-m-d"),
		"editrules" => array("edithidden" => true, "required" => true, "readonly" => false),
		"editoptions" => array("tabindex" => 7, "required" => true, "readonly" => false, "dataInit" =>
		"js:function(elm){setTimeout(function(){
                          jQuery(elm).datepicker({dateFormat:'yy-m-d'});
                             jQuery('.ui-datepicker').css({'zIndex':'1200','font-size':'75%'});},100);}")
	)
);

// Enable navigator
$grid->navigator = true;
// auto menutup setelah input data baru, edit data
$grid->setNavOptions('add', array("width" => "400", "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "closeAfterEdit" => true));
// Enable only deleting
$grid->setNavOptions('navigator', array("excel" => true, "pdf" => true, "add" => true, "edit" => true, "del" => true, "view" => false, "search" => false));
// Enjoy
$grid->renderGrid('#grid', '#pager', true, null, null, true, true);
$conn = null;
