<?php
require_once '../../../jq-config.php';
// include the jqGrid Class
require_once ABSPATH . "php/jqGrid.php";
// include the driver class
require_once ABSPATH . "php/jqGridPdo.php";
// Connection to the server
$conn = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
// Tell the db that we use utf-8
$conn->query("SET NAMES utf8");

// Create the jqGrid instance
$grid = new jqGridRender($conn);
// Write the SQL Query
$grid->SelectCommand = 'SELECT idbuku, judul, penulis, tglterbit, penerbit FROM buku';
// Set the table to where you add the data
$grid->table = 'buku';
$grid->setPrimaryKeyId('idbuku');
$grid->serialKey = false;
// Set output format to json
$grid->dataType = 'json';
// Let the grid create the model
$Model = array(
	array("name" => "idbuku", "label" => "ID Buku", "autowidth" => true, "hidden" => true),
	array("name" => "judul", "label" => "Judul", "autowidth" => true),
	array("name" => "penulis", "label" => "Penulis", "autowidth" => true),
	array("name" => "tglterbit", "label" => "Tanggal Terbit", "autowidth" => true),
	array("name" => "penerbit", "label" => "Penerbit", "autowidth" => true)
);
$grid->setColModel($Model);
// Set the url from where we obtain the data
$grid->setUrl('buku.php');
// Set some grid options
$grid->setGridOptions(array(
	"caption" => "BUKU PERPUSTAKAAN",
	"rowNum" => 10,
	"rowList" => array(10, 20, 30, 50, 100),
	"height" => 'auto',
	"hoverrows" => true,
	"autowidth" => true,
	"rownumbers" => true,
	"sortname" => "idbuku",
	"sortorder" => "asc",
	"altRows" => true,
	"altclass" => 'myAltRowClass'

));
// The primary key should be entered
$grid->setColProperty('idbuku', array("editrules" => array("required" => false)));

$grid->setDbDate('Y-m-d');
$grid->setDbTime('Y-m-d H:i:s');
$grid->setUserDate('Y-m-d');
$grid->setUserTime('Y-m-d');

$grid->setColProperty(
	'tglterbit',
	array(
		"formoptions" => array("label" => "tglterbit"), "editable" => true, "formatter" => "date", "datefmt" => 'd/m/Y', "formatoptions" => array("srcformat" => "Y-m-d H:i:s", "newformat" => "Y-m-d"),
		"editrules" => array("edithidden" => true, "required" => true, "readonly" => false),
		"editoptions" => array("tabindex" => 7, "required" => true, "readonly" => false, "dataInit" =>
		"js:function(elm){setTimeout(function(){
                          jQuery(elm).datepicker({dateFormat:'yy-m-d'});
                             jQuery('.ui-datepicker').css({'zIndex':'1200','font-size':'75%'});},100);}")
	)
);
// Enable navigator
$grid->navigator = true;
// auto menutup setelah input data baru, edit data
$grid->setNavOptions('add', array("width" => "400", "closeAfterAdd" => true));
$grid->setNavOptions('view', array("width" => "400", "datawidth" => "auto"));
$grid->setNavOptions('edit', array("width" => "400", "closeAfterEdit" => true));
// Enable only deleting
$grid->setNavOptions('navigator', array("excel" => false, "add" => true, "edit" => true, "del" => true, "view" => false, "search" => false));
// Enjoy
$grid->renderGrid('#grid', '#pager', true, null, null, true, true);
$conn = null;
